
(function() {

  var app = angular.module('myreddit', ['ionic', 'angularMoment']);

  //  Controllers
  app.controller('RedditCtrl', function($http, $scope) {

    $scope.stories = [];

    function loadStories(params, callback) {
      $http.get('https://www.reddit.com/r/Android/new/.json', {params: params})
        .success(function(response) {
          var stories = [];
          //  gets the json data and lists it
          angular.forEach(response.data.children, function(child) {
            console.log(child.data);
            stories.push(child.data);
          });
          callback(stories);
        });
    }
      // Show older data after pulling down
      $scope.loadOlderStories = function() {
        var params = {};
        if ($scope.stories.length > 0) {
          params['after'] = $scope.stories[$scope.stories.length - 1].name;
        }
        loadStories(params, function(olderStories) {
          $scope.stories = $scope.stories.concat(olderStories);
          $scope.$broadcast('scroll.infiniteScrollComplete');
        });
      };

      // Refresh page - pull up
      $scope.loadNewerStories = function() {
          var params = {'before': $scope.stories[0].name};
          loadStories(params, function(newerStories) {
            $scope.stories = newerStories.concat($scope.stories);
            $scope.$broadcast('scroll.refreshComplete');
          })
      };

      $scope.openLink = function(url) {
        window.open(url, '_blank');
      };
  });

  //  Main function
  app.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
      if(window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      // to work with this in cmd u have to install -> ionic plugin add cordova-plugin-inappbrowser
      if(window.cordova && window.cordova.InAppBrowser) {
        window.open = window.cordova.InAppBrowser.open;
      }
      if(window.StatusBar) {
        StatusBar.styleDefault();
      }
    });
  })
}());
